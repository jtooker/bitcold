BitCold
=======

Description
-----------
BitCold is an open source Android cold storage wallet for Bitcoin.

It is designed to work on two Android devices:

* Cold device
* Hot device

The cold device generates and stores keys.
Transactions are generated and submitted on the hot device,
but are signed on the cold devices.
All input/output on the cold device is done via QR code scanning.

The cold device may be encrypted or not.

Aside from holding Bitcoin, BitCold alerts you of all threats against
you coins.  Examples include: forgetting your password, not having
a backup, etcetera.

Private keys are stored in multiple places on the phone, including the SD card

How to Use
----------

_Development has not started yet, but this is the goal_

### Required Hardware ###

* __Cold Device:__ an old/cheap Android smart-phone with a camera and preferably an SD card
* __Hot Device:__ any android phone with network connection, presumably your main phone

### Setup ###

1. Install BitCold.apk on your cold device
1. Start BitCold on the cold device
1. Select that you want it as a cold device
    * Observe it goes into airplane mode
1. Choose whether you want to encrypt your keys
    * Encrypted keys cannot be recovered if you forget your password
    * Unencrypted keys cannot be protected if your device is stolen
1. Generate an address
1. Using any wallet on the hot device, send coins into cold storage
1. Wait...

### Spending ###
1. Install BitCold on your hot device
1. Scan your cold storage address
1. Generate a transaction
1. Scan the transaction with the cold device
1. Scan the signed transaction with the hot device and submit it to the network

### Optional Steps ###

* Build the .apk yourself from source using Eclipse
* Factory reset your cold device before using it
* Back up your cold device with another cold device
    * Requires additional devices

License
-------
Undetermined